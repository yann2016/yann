# yann

#### 项目介绍
yann

#### 软件架构
软件架构说明


#### 安装教程

1. 更改国内镜像composer config -g repo.packagist composer https://packagist.phpcomposer.com
2. 第一次执行 composer create-project topthink/think tp5  
   已经安装过执行 composer update topthink/framework
        
3. 七牛SDK composer require teg1c/thinkphp-qiniu-sdk
   如果该方法安装不成功，请在项目根目录下的composer.json的require中添加
   "teg1c/thinkphp-qiniu-sdk": "dev-master"

4. easywechat for thinkphp
   composer require naixiaoxin/think-wechat
   发送配置文件
   php think  wechat:config
   修改配置文件 修改项目根目录下config/wechat.php中对应的参数
   每个模块基本都支持多账号，默认为 default
   
#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

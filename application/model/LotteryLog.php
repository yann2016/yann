<?php
/**
 * Author: yann
 * QQ: 9197313
 * Date: 2018/11/29 0029
 * Time: 下午 2:06
 */

namespace app\model;


class LotteryLog extends Base
{
    public function getList($lid,$page,$size){
        try{
            $list = $this->view('LotteryLog','id,lid,openid,code,phone,address,remark,name,status')
                ->view('MpFans','nickname,headimgurl','LotteryLog.openid=MpFans.openid')
                ->where('LotteryLog.lid','=',$lid)
                ->limit(($page - 1)*$size,$size)
                ->select()->toArray();
            return [
                'list' => $list,
                'count' => $this->where(['lid'=>$lid])->count()
            ];
        }catch (\Exception $e){
            return [
                'list' => [],
                'count' => 0
            ];
        }
    }

    public function getMyOpen($openid,$page,$size){
        try{
            $list = $this->view('LotteryLog','lid')
                ->view('Lottery','title,end_time,status','LotteryLog.lid=Lottery.id')
                ->where('LotteryLog.openid','=',$openid)
                ->order(['LotteryLog.id'=>'desc'])
                ->limit(($page - 1)*$size,$size)
                ->select()->toArray();
            return [
                'list' => $list,
                'count' => $this->where(['openid'=>$openid])->count()
            ];
        }catch (\Exception $e){
            return [
                'list' => [],
                'count' => 0
            ];
        }
    }
}

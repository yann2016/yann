<?php
/**
 * Author: yann
 * QQ: 9197313
 * Date: 2018/11/20 0020
 * Time: 下午 4:45
 */

namespace app\model;



class Lottery extends Base
{
    public function getList($where = 1, $page = 1, $size = 10, $order = ['id'=>'desc']){
        try {
            $list = $this->where($where)->order($order)->limit(($page - 1)*$size,$size)->select()->toArray();
            return [
                'list' =>  $list,
                'count' => $this->where($where)->count()
            ];
        } catch (\Exception $e) {
            return [
                'list' => [],
                'count' => 0
            ];
        }
    }

    public function getInfo($id,$openid){

        try{
            $info = $this->where(['id'=>$id])->find();
            $info['count'] = LotteryAttend::where(['lid'=>$id])->count();
            $attendee = [];
            $tmp = $this->view('LotteryAttend','id')
                ->view('MpFans','headimgurl','LotteryAttend.openid=MpFans.openid')
                ->where(['LotteryAttend.lid'=>$id,'LotteryAttend.openid'=>$openid])->find();
            $temp = $this->view('LotteryAttend','id')
                ->view('MpFans','headimgurl','LotteryAttend.openid=MpFans.openid')
                ->where([['LotteryAttend.lid','=',$id],['LotteryAttend.openid','<>',$openid]])
                ->order(['LotteryAttend.id'=>'desc'])
                ->limit(0,4)
                ->select()->toArray();
            if($tmp){
                $attendee[] = $tmp;
            }

            $attendee = array_merge($attendee,$temp);
            foreach ($attendee as $key => $value) {
                $attendee[$key]['num'] = LotteryCode::where(['attend_id'=>$value['id']])->count();
            }
            $info['attendee'] = $attendee;
            $info['isattend'] = LotteryAttend::where(['lid'=>$info['id'],'openid'=>$openid])->count();
            $info['check'] = (new MpFans())->checkInfo($openid);

            if($info['status'] > 1){
                $info['isopen'] = LotteryLog::where(['openid'=>$openid,'lid'=>$id])->count();
                $open = $this->view('LotteryLog','code')
                    ->view('MpFans','headimgurl,nickname','MpFans.openid=LotteryLog.openid')
                    ->where(['LotteryLog.lid'=>$id])->select()->toArray();
                $info['open'] = $open;
            }
            return $info;
        }catch (\Exception $e){
            return [];
        }
    }

}

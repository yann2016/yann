<?php
/**
 * Author: yann
 * QQ: 9197313
 * Date: 2018/11/1 0001
 * Time: 上午 10:08
 */

namespace app\model;


class Article extends Base
{
    public function getList($where = 1, $page = 1, $size = 10, $order = ['id'=>'desc']){
        try {
            $list = $this->where($where)->order($order)->limit(($page - 1)*$size,$size)->select()->toArray();
            return [
                'list' =>  $list,
                'count' => $this->where($where)->count()
            ];
        } catch (\Exception $e) {
            return [
                'list' => [],
                'count' => 0
            ];
        }
    }
}

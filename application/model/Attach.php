<?php
namespace app\model;



class Attach extends Base {
    const LOCALTYPE = 1;
    const QINIUTYPE = 2;

    public function getList($where = 1, $page = 1, $size = 10, $order = ['id'=>'desc']){
        try {
            $list = $this->where($where)->order($order)->limit(($page - 1)*$size,$size)->select()->toArray();
            return [
                'list' =>  $list,
                'count' => $this->where($where)->count()
            ];
        } catch (\Exception $e) {
            return [
                'list' => [],
                'count' => 0
            ];
        }
    }
}

<?php
/**
 * Author: yann
 * QQ: 9197313
 * Date: 2019/1/2 0002
 * Time: 下午 3:16
 */

namespace app\model;


class BrandArticle extends Base
{
    public function getList($where = 1, $page = 1, $size = 10, $order = ['BrandArticle.id'=>'desc']){
        try {
            $list = $this->view('BrandArticle','id,title,create_time,sort,status')
                ->view('BrandCate','title as cate_name','BrandArticle.cate_id=BrandCate.id')
                ->where($where)
                ->order($order)
                ->limit(($page - 1)*$size,$size)
                ->select()
                ->toArray();
            return [
                'list' =>  $list,
                'count' => $this->where($where)->count()
            ];
        } catch (\Exception $e) {
            return [
                'list' => [],
                'count' => 0
            ];
        }
    }
}

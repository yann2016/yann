<?php
/**
 * Author: yann
 * QQ: 9197313
 * Date: 2018/12/14 0014
 * Time: 上午 10:52
 */

namespace app\model;


class AuthGroupAccess extends Base
{
    public function del($gid,$uid){
        try{
            $oldInfo = $this->where(['uid' => $uid])->find();
            $oldGroupArr = explode(',', $oldInfo['group_id']);
            $key = array_search($gid, $oldGroupArr);
            unset($oldGroupArr[$key]);
            $newData = implode(',', $oldGroupArr);
            return $this->update(['group_id' => $newData], ['uid' => $uid]);
        }catch(\Exception $e){
            return false;
        }
    }
}

<?php
namespace app\model;



use app\util\ReturnCode;
use app\util\Tools;

class Admin extends Base{

    public function getList($where = 1, $page= 1, $size= 10, $order = ['id'=>'asc']){
        try {
            $list = $this->where($where)->order($order)->limit(($page - 1)*$size,$size)->select()->toArray();
            foreach ($list as $key => $value){
                $group_id = AuthGroupAccess::where(['uid'=>$value['id']])->value('group_id');
                $list[$key]['group_id'] = $group_id ? explode(',',$group_id) : [];
            }
            return [
                'list' =>  $list,
                'count' => $this->where($where)->count()
            ];
        } catch (\Exception $e) {
            return [
                'list' => [],
                'count' => 0
            ];
        }
    }


    //获取当前用户组成员
    public function getAdmin($page, $size, $gid){
        try{
            $listInfo = AuthGroupAccess::where('group_id','like', "%{$gid}%")->select()->toArray();
            $uidArr = array_column($listInfo, 'uid');
            $list = $this->where('id','in',$uidArr)->order('id')->limit(($page - 1)*$size,$size)->select()->toArray();

            return [
                'list'  => $list,
                'count' => $this->where('id','in',$uidArr)->count()
            ];
        }catch (\Exception $e){
            return [
                'list'  => [],
                'count' => 0
            ];
        }
    }

    /**
     * @param $data
     * @return array
     * @throws \think\exception\PDOException
     */
    public function adminEdit($data){
        $groups = '';
        if ($data['group_id']) {
            $groups = trim(implode(',', $data['group_id']), ',');
        }
        unset($data['group_id']);
        $this->startTrans();
        try{
            if($data['id']){
                if(isset($data['password']) && ($data['password'] !='YCMS'))
                    $data['password'] = Tools::userMd5($data['password']);
                else
                    unset($data['password']);

                Admin::update($data);
                if (AuthGroupAccess::where(['uid' => $data['id']])->count())
                    AuthGroupAccess::update(['group_id' => $groups], ['uid' => $data['id']]);
                else
                    AuthGroupAccess::create(['uid'=> $data['id'], 'group_id' => $groups]);
                $this->commit();
                return ['code' => 1, 'msg'=> '操作成功'];
            }else{
                if(Admin::where(['username' => $data['username']])->count())
                    return ['code' => ReturnCode::REPEAT_OF_USERNAME, 'msg'=> '用户名重复'];

                $data['password'] = Tools::userMd5($data['password']);
                $data['create_time'] = time();
                $res = Admin::create($data);
                AuthGroupAccess::create(['uid'=> $res->id, 'group_id' => $groups]);
                $this->commit();
                return ['code' => 1, 'msg'=> '操作成功'];
            }

        }catch (\Exception $e){
            $this->rollback();
            return ['code' => ReturnCode::DB_SAVE_ERROR, 'msg'=> '操作失败'];
        }
    }

    /**
     * @param $id
     * @return bool
     * @throws \think\exception\PDOException
     */
    public function adminDel($id){
        $this->startTrans();
        try{
            Admin::destroy($id);
            AuthGroupAccess::destroy(['uid' => $id]);
           $this->commit();
            return true;
        }catch(\Exception $e){
            $this->rollback();
            return false;
        }
    }
}

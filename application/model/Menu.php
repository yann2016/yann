<?php
/**
 * Author: yann
 * QQ: 9197313
 * Date: 2018/12/14 0014
 * Time: 上午 11:51
 */

namespace app\model;


use app\util\Tools;

class Menu extends Base
{
    public function getList() {
        try{
            $list = $this->order('sort', 'ASC')->select()->toArray();
            return Tools::formatTree(Tools::listToTree($list));
        }catch (\Exception $e){
            return [];
        }
    }
}

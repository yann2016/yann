<?php
/**
 * Author: yann
 * QQ: 9197313
 * Date: 2018/12/14 0014
 * Time: 上午 10:51
 */

namespace app\model;


class AuthGroup extends Base
{
    public function rules() {
        return $this->hasMany('AuthRule', 'group_id', 'id');
    }

    public function getList($where = 1, $page = 1, $size = 10, $order = ['id'=>'asc']){
        try {
            $list = $this->where($where)->order($order)->limit(($page - 1)*$size,$size)->select()->toArray();
            return [
                'list' =>  $list,
                'count' => $this->where($where)->count()
            ];
        } catch (\Exception $e) {
            return [
                'list' => [],
                'count' => 0
            ];
        }
    }

    public function getGroup($where){
        try {
            $list = $this->where($where)->order(['id'=>'desc'])->select()->toArray();
            return [
                'list' =>  $list,
                'count' => $this->where($where)->count()
            ];
        } catch (\Exception $e) {
            return [
                'list' => [],
                'count' => 0
            ];
        }
    }

    /**
     * @param $postData
     * @return bool
     * @throws \think\exception\PDOException
     */
    public function addGroup($postData){
        $rules = [];
        if ($postData['rules']) {
            $rules = $postData['rules'];
            $rules = array_filter($rules);
        }
        unset($postData['rules']);
        $this->startTrans();
        try{
            $id = $this->insertGetId(['name'=>$postData['name'],'description'=>$postData['description']]);
            if ($rules) {
                $insertData = [];
                foreach ($rules as $value) {
                    if ($value) {
                        $insertData[] = [
                            'group_id' => $id,
                            'url'     => $value
                        ];
                    }
                }
                (new AuthRule())->saveAll($insertData);
            }
            $this->commit();
            return true;
        }catch (\Exception $e){
            $this->rollback();
            return false;
        }
    }

    /**
     * @param $id
     * @return bool
     * @throws \think\exception\PDOException
     */
    public function delGroup($id){
        $this->startTrans();
        try{
            $listInfo = AuthGroupAccess::where('group_id' ,'like', "%{$id}%")->select();
            if ($listInfo) {
                foreach ($listInfo as $value) {
                    $valueArr = $value->toArray();
                    $oldGroupArr = explode(',', $valueArr['group_id']);
                    $key = array_search($id, $oldGroupArr);
                    unset($oldGroupArr[$key]);
                    $newData = implode(',', $oldGroupArr);
                    $value->group_id = $newData;
                    $value->save();
                }
            }
            $this->destroy($id);
            AuthRule::destroy(['group_id' => $id]);
            $this->commit();
            return true;
        }catch (\Exception $e){
            $this->rollback();
            return false;
        }
    }
}

<?php
/**
 * Author: yann
 * QQ: 9197313
 * Date: 2019/1/2 0002
 * Time: 下午 3:16
 */

namespace app\model;


class BrandCate extends Base
{
    public function getList($where = 1, $page = 1, $size = 10, $order = ['id'=>'desc']){
        try {
            $list = $this->where($where)->order($order)->limit(($page - 1)*$size,$size)->select()->toArray();
            return [
                'list' =>  $list,
                'count' => $this->where($where)->count()
            ];
        } catch (\Exception $e) {
            return [
                'list' => [],
                'count' => 0
            ];
        }
    }
}

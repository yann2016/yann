<?php
/**
 * Author: yann
 * QQ: 9197313
 * Date: 2018/11/27 0027
 * Time: 上午 11:12
 */

namespace app\model;



class MpFans extends Base
{
    public function getList($where = 1, $page = 1, $size = 10, $order = ['id'=>'desc']){
        try {
            $list = $this->where($where)->order($order)->limit(($page - 1)*$size,$size)->select()->toArray();
            return [
                'list' =>  $list,
                'count' => $this->where($where)->count()
            ];
        } catch (\Exception $e) {
            return [
                'list' => [],
                'count' => 0
            ];
        }
    }
    public function checkInfo($openid){
        try{
            $res = $this->where(['openid'=>$openid])->find();
        }catch (\Exception $e){
            $res['nickname'] = '';
        }
        return $res['nickname'] != '' ? 1 : 0;
    }
}

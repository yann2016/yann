<?php
/**
 * Author: yann
 * QQ: 9197313
 * Date: 2018/11/29 0029
 * Time: 下午 1:57
 */
namespace app\model;



class LotteryCode extends Base
{
    /**
     * @param $attend_id
     * @param $aid
     * @param $lid
     * @return bool|int|mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function addCode($attend_id,$aid,$lid){

        if(!$this->where(['attend_id'=>$attend_id,'aid'=>$aid,'lid'=>$lid])->count()){
            $appoint = Lottery::where(['id'=>$lid])->value('appoint');
            $appoint = explode("\n",$appoint);
            $code = self::getCode($lid);
            if (count($appoint) > 0) {
                while (in_array($code, $appoint)) {
                    $openid = self::addXuni($lid);

                    $attend = LotteryAttend::create([
                        'lid' => $lid,
                        'openid' => $openid,
                        'form_id' => 'the formId is a mock one'
                    ]);

                    LotteryCode::create([
                        'code' => $code,
                        'attend_id' => $attend['id'],
                        'aid' => $openid,
                        'lid' => $lid
                    ]);
                    $code = self::getCode($lid);
                }
            }

            LotteryCode::create([
                'code' => $code,
                'attend_id' => $attend_id,
                'aid' => $aid,
                'lid' => $lid
            ]);

            return $code;

        }else{
            return false;
        }
    }

    /**
     * @param $attend_id
     * @param $aid
     * @param $lid
     * @return bool|int|mixed
     * @throws \think\exception\PDOException
     */
    public function assistCode($attend_id,$aid,$lid){
        $this->startTrans();
        try{
            $code = self::addCode($attend_id,$aid,$lid);
            $this->commit();
            return $code;
        }catch (\Exception $e){
            $this->rollback();
            return false;
        }
    }

    /**
     * @param $lid
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    private function addXuni($lid){
        $info = LotteryXuni::where(['status' => 0])->find();

        $mpfans = MpFans::create([
            'openid' => 'xuni_user_id_'.$lid.'_'.$info['id'],
            'headimgurl' => $info['headimgurl'],
            'nickname' => $info['nickname'],
        ]);
        LotteryXuni::update(['status'=>1],['id'=>$info['id']]);
        return $mpfans['openid'];
    }

    /**
     * @param $lid
     * @return int|mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    private function getCode($lid){
        $last = LotteryCode::where(['lid' => $lid])->order(['id' => 'desc'])->find();
        if($last)
            $code = $last['code'] + 1;
        else
            $code = 10000001;

        return $code;
    }
}

<?php
/**
 * Author: yann
 * QQ: 9197313
 * Date: 2018/11/29 0029
 * Time: 下午 12:05
 */

namespace app\model;


use think\Db;

class LotteryAttend extends Base
{
    public function LotteryCode()
    {
        return $this->hasMany('LotteryCode','attend_id');
    }

    public function getList($data, $page = 1, $size = 10, $order = ['id'=>'desc']){
        try {
            $tmp = [];
            if($page == 1){
                $tmp = LotteryAttend::withCount('LotteryCode')
                    ->where('lid','=',$data['lid'])
                    ->where('openid','=',$data['openid'])
                    ->select()->toArray();
            }

            $list = $this->withCount('LotteryCode')
                ->where('lid','=',$data['lid'])
                ->where('openid','<>',$data['openid'])
                ->order($order)->limit(($page - 1)*$size,$size)->select()->toArray();
            $list = array_merge($tmp,$list);
            $total_code = LotteryCode::where(['lid'=>$data['lid']])->count();
            foreach ($list as $key => $value){
                $mpFans = MpFans::where(['openid'=>$value['openid']])->field('headimgurl,nickname')->find();
                $code = $this->view('LotteryCode','code')
                    ->view('MpFans','headimgurl','LotteryCode.aid=MpFans.openid')
                    ->where('LotteryCode.attend_id','=',$value['id'])->select()->toArray();
                $list[$key]['headimgurl'] = $mpFans['headimgurl'];
                $list[$key]['nickname'] = $mpFans['nickname'];
                $list[$key]['code'] = $code;
                $list[$key]['gailv'] = count($code) > 1 ?  number_format(((count($code)/$total_code) - (1/($total_code - count($code) +1)))*100,2,".",""): '0.00';
            }
            return [
                'list' =>  $list,
                'count' => $this->where('lid','=',$data['lid'])->count()
            ];
        } catch (\Exception $e) {
            return [
                'list' => [],
                'count' => 0
            ];
        }
    }

    /**
     * @param $data
     * @param $openid
     * @param $lid
     * @return array|bool
     * @throws \think\exception\PDOException
     */
    public function attend($data,$openid,$lid){
        $this->startTrans();
        try{
            $attend_id = $this->insert($data,false,true);
            $code = (new LotteryCode())->addCode($attend_id,$openid,$lid);
            $this->commit();
            return ['attend_id'=>$attend_id,'code'=>$code];
        }catch (\Exception $e){
            $this->rollback();
            return false;
        }
    }

    public function getMyAttend($openid,$page = 1,$size = 20){
        try{
            $list = $this->view('LotteryAttend','id,lid')
                ->view('Lottery','title,end_time,status','LotteryAttend.lid=Lottery.id')
                ->where('LotteryAttend.openid','=',$openid)
                ->order(['LotteryAttend.id'=>'desc'])
                ->limit(($page - 1)*$size,$size)
                ->select()->toArray();
            return [
                'list' => $list,
                'count' => $this->where(['openid'=>$openid])->count()
            ];
        }catch (\Exception $e){
            return [
                'list' => [],
                'count' => 0
            ];
        }
    }

    public function getWinners(){
        try{
            $list = $this->view('LotteryAttend','id')
                ->view('MpFans','headimgurl,nickname','MpFans.openid=LotteryAttend.openid')
                ->order(['LotteryAttend.id'=>'desc'])
                ->limit(0,20)
                ->select()->toArray();
        }catch (\Exception $e){
            $list = [];
        }
        return $list;
    }
}

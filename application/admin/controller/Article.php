<?php
/**
 * Author: yann
 * QQ: 9197313
 * Date: 2018/11/1 0001
 * Time: 上午 10:04
 */

namespace app\admin\controller;


use app\model\Article as _Article;
use app\model\ArticleCate;
use app\util\ReturnCode;

class Article extends Base
{
    protected $article;
    protected $articleCate;

    public function __construct(_Article $article,ArticleCate $articleCate)
    {
        parent::__construct();
        $this->article = &$article;
        $this->articleCate = &$articleCate;
    }

    /**
     * 文章列表
     * @return false|string
     */
    public function getList(){
        $page = $this->request->post('page');
        $size = $this->request->post('size');
        $list = $this->article->getList(1,$page,$size);
        return $this->buildSuccess($list);
    }

    /**
     * 文章详细
     * @return false|string
     */
    public function getInfo(){
        $id = $this->request->post('id',0);
        if(!$id)
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS, '参数错误');
        try{
            $info = $this->article->where(['id'=>$id])->find();
            return $this->buildSuccess($info);
        }catch (\Exception $e){
            return $this->buildFailed($e->getCode(),$e->getMessage());
        }
    }

    /**
     * 编辑文章
     * @return false|string
     */
    public function articleSave(){
        $data = $this->request->post();
        $id = $this->request->post('id',0);
        if($id)
            $res = $this->article->allowField(true)->save($data,['id'=>$id]);
        else
            $res = $this->article->insert($data);

        if(!$res)
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS ,'参数错误');

        return $this->buildSuccess([]);
    }

    /**
     * 删除文章
     * @return string
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function articleDel(){
        $id = $this->request->post('id');
        if(!$id)
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS,'参数错误');
        $this->article->where(['id'=>$id])->delete();
        return $this->buildSuccess([]);
    }

    /**
     * 分类列表
     * @return false|string
     */
    public function getCategory(){
        $page = $this->request->post('page');
        $size = $this->request->post('size');
        $list = $this->article->getList(1,$page,$size);
        return $this->buildSuccess($list);
    }

    /**
     * 编辑分类
     * @return false|string
     */
    public function categorySave(){
        $data = $this->request->post();
        $id = $this->request->post('id',0);
        if($id)
            $res = $this->articleCate->allowField(true)->save($data,['id'=>$id]);
        else
            $res = $this->articleCate->insert($data);

        if(!$res)
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS,'参数错误');

        return $this->buildSuccess([]);
    }

    /**
     * 删除分类
     * @return string
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function categoryDel(){
        $id = $this->request->post('id');
        if(!$id)
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS,'参数错误');
        $this->articleCate->where(['id'=>$id])->delete();

        return $this->buildSuccess([]);
    }
}

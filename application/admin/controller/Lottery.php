<?php
/**
 * Author: yann
 * QQ: 9197313
 * Date: 2018/11/20 0020
 * Time: 下午 4:44
 */

namespace app\admin\controller;
use app\model\Lottery as _Lottery;
use app\model\LotteryLog;
use app\model\LotteryXuni;
use app\util\ReturnCode;

class Lottery extends Base
{

    protected $lottery;
    protected $lotteryLog;
    protected $lotteryXuni;

    public function __construct(
        _Lottery $lottery,
        LotteryLog $lotteryLog ,
        LotteryXuni $lotteryXuni
    )
    {
        parent::__construct();
        $this->lottery = &$lottery;
        $this->lotteryLog = &$lotteryLog;
        $this->lotteryXuni = &$lotteryXuni;
    }

    /**
     * 抽奖活动列表
     * @return false|string
     */
    public function getList(){
        $page = $this->request->post('page',1);
        $size = $this->request->post('size',20);
        $list = $this->lottery->getList(1,$page,$size);
        return $this->buildSuccess($list);
    }

    /**
     * 抽奖活动详细
     * @return false|string
     */
    public function getInfo(){
        $id = $this->request->post('id',0);
        if(!$id)
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS, '参数错误');
        try{
            $info = $this->lottery->where(['id'=>$id])->find();
            return $this->buildSuccess($info);
        }catch (\Exception $e){
            return $this->buildFailed($e->getCode(),$e->getMessage());
        }
    }

    /**
     * 编辑抽奖活动
     * @return false|string
     */
    public function lotterySave(){
        $data = $this->request->post();
        if(isset($data['start_time']) && isset($data['end_time'])){
            $data['start_time'] = strtotime($data['start_time']);
            $data['end_time'] = strtotime($data['end_time']);
        }

        $id = $this->request->post('id',0);
        if($id)
            $res = $this->lottery->allowField(true)->save($data,['id'=>$id]);
        else
            $res = $this->lottery->insert($data);

        if(!$res)
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS ,'参数错误');

        return $this->buildSuccess([]);
    }

    /**
     *
     * 删除抽奖活动
     * @return string
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function lotteryDel(){
        $id = $this->request->post('id');
        if(!$id)
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS,'参数错误');
        $this->lottery->where(['id'=>$id])->delete();
        return $this->buildSuccess([]);
    }

    /**
     * 中奖记录
     * @return false|string
     */
    public function lucky(){
        $lid = $this->request->post('lid',0);
        if(!$lid)
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS,'参数错误');

        $page = $this->request->post('page',1);
        $size = $this->request->post('size',20);
        $list = $this->lotteryLog->getList($lid,$page,$size);
        return $this->buildSuccess($list);
    }

    /**
     * 中奖记录状态
     * @return false|string
     */
    public function luckySave(){
        $id = $this->request->post('id');
        $status = $this->request->post('status');
        if(!$id)
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS,'参数错误');

        $this->lotteryLog->save(['status'=>$status],['id'=>$id]);
        return $this->buildSuccess([]);
    }

    /**
     * 虚拟用户列表
     * @return false|string
     */
    public function virtual(){
        $page = $this->request->post('page',1);
        $size = $this->request->post('size',20);
        $list = $this->lotteryXuni->getList(1,$page,$size);
        return $this->buildSuccess($list);
    }

    /**
     * 虚拟用户编辑
     * @return false|string
     */
    public function virtualSave(){
        $data = $this->request->post();
        $id = $this->request->post('id');
        if($id){
            if($this->lotteryXuni->allowField(true)->save($data,['id'=>$id]))
                return $this->buildSuccess([]);
            else
                return $this->buildFailed(ReturnCode::DB_SAVE_ERROR,'操作失败');
        } else {
            if($this->lotteryXuni->insert($data))
                return $this->buildSuccess([]);
            else
                return $this->buildFailed(ReturnCode::DB_SAVE_ERROR,'操作失败');
        }
    }

    /**
     * 删除虚拟用户
     * @return string
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function virtualDel(){
        $id = $this->request->post('id');
        if(!$id)
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS,'参数错误');
        $this->lotteryXuni->where(['id'=>$id])->delete();
        return $this->buildSuccess([]);
    }

    /**
     *
     * 更改虚拟用户状态
     * @return string
     */
    public function virtualStatus(){
        $id = $this->request->post('id');
        $status = $this->request->post('status');
        if(!$id){
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS ,'参数错误');
        }
        if($this->lotteryXuni->save(['status'=>$status],['id'=>$id]))
            return $this->buildSuccess([]);
        else
            return $this->buildFailed(ReturnCode::DB_SAVE_ERROR ,'操作失败');
    }
}

<?php
/**
 * Author: yann
 * QQ: 9197313
 * Date: 2018/12/14 0014
 * Time: 上午 10:55
 */

namespace app\admin\controller;

use app\model\AuthGroup;
use app\model\AuthGroupAccess;
use app\model\AuthRule;
use app\util\ReturnCode;

class Auth extends Base
{

    protected $authGroup;
    protected $authGroupAccess;
    protected $authRule;

    public function __construct(
        AuthGroup $authGroup,
        AuthGroupAccess $authGroupAccess,
        AuthRule $authRule
    )
    {
        parent::__construct();
        $this->authGroup = &$authGroup;
        $this->authGroupAccess = &$authGroupAccess;
        $this->authRule = &$authRule;
    }

    /**
     * @return false|string
     */
    public function index(){
        $page = $this->request->post('page',1);
        $size = $this->request->post('size',20);
        $return = $this->authGroup->getList(1,$page,$size);
        return $this->buildSuccess($return);
    }

    /**
     * 获取权限组列表
     * @return false|string
     */
    public function getGroups() {
        $return = $this->authGroup->getList(['status'=>1]);
        return $this->buildSuccess($return);
    }

    /**
     * 获取权限列表
     * @return false|string
     */
    public function getRuleList() {
        $group_id = $this->request->post('group_id', 0);
        $list = $this->authRule->getList($group_id);
        return $this->buildSuccess(['list' => $list]);
    }

    /**
     * 添加权限组
     * @return false|string
     * @throws \think\exception\PDOException
     */
    public function add() {
        $postData = $this->request->post();
        if($this->authGroup->addGroup($postData))
            return $this->buildSuccess([]);
        else
            return $this->buildFailed(ReturnCode::DB_SAVE_ERROR, '操作失败');
    }

    /**
     * 编辑用户权限
     * @return false|string
     * @throws \think\exception\PDOException
     */
    public function edit() {
        $postData = $this->request->post();
        if ($this->authRule->editRule($postData))
            return $this->buildSuccess([]);
        else
            return $this->buildFailed(ReturnCode::DB_SAVE_ERROR, '操作失败');
    }

    /**
     * 权限组状态编辑
     * @return false|string
     */
    public function changeStatus() {
        $id = $this->request->post('id');
        $status = $this->request->post('status');

        if ($this->authGroup->save(['status'=>$status],['id'=>$id]))
            return $this->buildSuccess([]);
        else
            return $this->buildFailed(ReturnCode::DB_SAVE_ERROR, '操作失败');
    }

    /**
     * 删除组
     * @return false|string
     * @throws \think\exception\PDOException
     */
    public function del() {
        $id = $this->request->post('id');
        if (!$id)
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS, '参数错误');

        if($this->authGroup->delGroup($id))
            return $this->buildSuccess([]);
        else
            return $this->buildFailed(ReturnCode::DB_SAVE_ERROR, '操作失败');
    }

    /**
     * 从指定组中删除指定用户
     * @return false|string
     */
    public function delMember() {
        $gid = $this->request->post('gid', 0);
        $uid = $this->request->post('uid', 0);
        if (!$gid || !$uid)
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS, '参数错误');

        if ($this->authGroupAccess->del($gid,$uid))
            return $this->buildSuccess([]);
        else
            return $this->buildFailed(ReturnCode::DB_SAVE_ERROR, '操作失败');
    }

}

<?php
/**
 * Author: yann
 * QQ: 9197313
 * Date: 2019/1/2 0002
 * Time: 上午 11:54
 */

namespace app\admin\controller;


use app\model\BrandArticle;
use app\model\BrandCate;
use app\model\BrandPhoto;
use app\model\BrandVideo;
use app\util\ReturnCode;

class Brand extends Base
{

    protected $brandArticle;
    protected $brandCate;
    protected $brandPhoto;
    protected $brandVideo;

    public function __construct(
        BrandArticle $brandArticle,
        BrandCate $brandCate,
        BrandPhoto $brandPhoto,
        BrandVideo $brandVideo
    )
    {
        parent::__construct();
        $this->brandArticle = &$brandArticle;
        $this->brandCate = &$brandCate;
        $this->brandPhoto = &$brandPhoto;
        $this->brandVideo = &$brandVideo;
    }

    /**
     * 分类列表
     * @return false|string
     */
    public function category(){
        $page = $this->request->post('page');
        $size = $this->request->post('size');
        $list = $this->brandCate->getList(1,$page,$size);
        return $this->buildSuccess($list);
    }

    /**
     * 编辑分类
     * @return false|string
     */
    public function categorySave(){
        $data = $this->request->post();
        $id = $this->request->post('id',0);
        if($id)
            $res = $this->brandCate->allowField(true)->save($data,['id'=>$id]);
        else
            $res = $this->brandCate->insert($data);

        if(!$res)
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS ,'参数错误');

        return $this->buildSuccess([]);
    }

    /**
     * 删除分类
     * @return string
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function categoryDel(){
        $id = $this->request->post('id');
        if(!$id)
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS,'参数错误');
        $this->brandCate->where(['id'=>$id])->delete();
        return $this->buildSuccess([]);
    }

    /**
     * 文章列表
     * @return false|string
     */
    public function article(){
        $page = $this->request->post('page');
        $size = $this->request->post('size');
        $list = $this->brandArticle->getList(1,$page,$size);
        return $this->buildSuccess($list);
    }

    /**
     * 文章详细
     * @return string
     */
    public function getArticle(){
        $id = $this->request->post('id',0);
        if(!$id)
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS, '参数错误');
        try{
            $info = $this->brandArticle->where(['id'=>$id])->find($id);
            return $this->buildSuccess($info);
        }catch (\Exception $e){
            return $this->buildFailed($e->getCode(),$e->getMessage());
        }
    }

    /**
     * 编辑文章
     * @return false|string
     */
    public function articleSave(){
        $data = $this->request->post();
        $id = $this->request->post('id',0);
        if($id){
            $res = $this->brandArticle->allowField(true)->save($data,['id'=>$id]);
        }
        else{
            $data['create_time'] = time();
            $res = $this->brandArticle->insert($data);
        }

        if(!$res)
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS ,'参数错误');

        return $this->buildSuccess([]);
    }

    /**
     * 删除文章
     * @return string
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function articleDel(){
        $id = $this->request->post('id');
        if(!$id)
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS,'参数错误');
        $this->brandArticle->where(['id'=>$id])->delete();
        return $this->buildSuccess([]);
    }

    /**
     * 图集列表
     * @return false|string
     */
    public function photo(){
        $page = $this->request->post('page');
        $size = $this->request->post('size');
        $list = $this->brandPhoto->getList(1,$page,$size);
        return $this->buildSuccess($list);
    }

    /**
     * 图集详细
     * @return false|string
     */
    public function getPhoto(){
        $id = $this->request->post('id',0);
        if(!$id)
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS, '参数错误');
        try{
            $info = $this->brandPhoto->where(['id'=>$id])->find();
            return $this->buildSuccess($info);
        }catch (\Exception $e){
            return $this->buildFailed($e->getCode(),$e->getMessage());
        }
    }

    /**
     * 编辑图集
     * @return false|string
     */
    public function photoSave(){
        $data = $this->request->post();
        $id = $this->request->post('id',0);
        if($id){
            $res = $this->brandPhoto->allowField(true)->save($data,['id'=>$id]);
        }else{
            $data['create_time'] = time();
            $res = $this->brandPhoto->insert($data);
        }

        if(!$res)
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS ,'参数错误');

        return $this->buildSuccess([]);
    }

    /**
     * 删除图集
     * @return string
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function photoDel(){
        $id = $this->request->post('id');
        if(!$id)
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS,'参数错误');
        $this->brandPhoto->where(['id'=>$id])->delete();
        return $this->buildSuccess([]);
    }

    /**
     * 视频列表
     * @return false|string
     */
    public function video(){
        $page = $this->request->post('page');
        $size = $this->request->post('size');
        $list = $this->brandVideo->getList(1,$page,$size);
        return $this->buildSuccess($list);
    }

    /**
     *
     * 视频详细
     * @return string
     */
    public function getVideo(){
        $id = $this->request->post('id',0);
        if(!$id)
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS, '参数错误');
        try{
            $info = $this->brandVideo->where(['id'=>$id])->find();
            return $this->buildSuccess($info);
        }catch (\Exception $e){
            return $this->buildFailed($e->getCode(),$e->getMessage());
        }
    }

    /**
     * 编辑视频
     * @return false|string
     */
    public function videoSave(){
        $data = $this->request->post();
        $id = $this->request->post('id',0);
        if($id) {
            $res = $this->brandVideo->allowField(true)->save($data,['id'=>$id]);
        }
        else{
            $data['create_time'] = time();
            $res = $this->brandVideo->insert($data);
        }
        if(!$res)
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS ,'参数错误');

        return $this->buildSuccess([]);
    }

    /**
     * 删除视频
     * @return string
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function videoDel(){
        $id = $this->request->post('id');
        if(!$id)
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS,'参数错误');
        $this->brandVideo->where(['id'=>$id])->delete();
        return $this->buildSuccess([]);
    }

    /**
     * 获取分类
     * @return string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getType(){
        $type = $this->request->post('type',1);
        $list = $this->brandCate->where(['type'=>$type])->select()->toArray();
        return $this->buildSuccess($list);
    }
}

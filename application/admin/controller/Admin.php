<?php
/**
 * Author: yann
 * QQ: 9197313
 * Date: 2018/12/19 0019
 * Time: 上午 11:55
 */

namespace app\admin\controller;

use app\model\Admin as _Admin;
use app\util\ReturnCode;

class Admin extends Base
{
    protected $admin;

    public function __construct(_Admin $admin)
    {
        parent::__construct();
        $this->admin = &$admin;
    }

    /**
     * 管理员列表
     * @return false|string
     */
    public function adminList(){
        $page = $this->request->post('page',1);
        $size = $this->request->post('size',10);
        $data = $this->admin->getList(1,$page, $size);
        return $this->buildSuccess($data);
    }

    /**
     * 编辑管理员
     * @return false|string
     * @throws \think\exception\PDOException
     */
    public function edit(){
        $data = $this->request->post();
        $res = $this->admin->adminEdit($data);
        if($res['code'] === 1)
            return $this->buildSuccess([]);
        else
            return $this->buildFailed($res['code'],$res['msg']);
    }

    /**
     * 删除管理员
     * @return false|string
     * @throws \think\exception\PDOException
     */
    public function del(){
        $id = $this->request->post('id');
        if (!$id)
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS, '参数错误');

        if($this->admin->adminDel($id))
            return $this->buildSuccess([]);
        else
            return $this->buildFailed(ReturnCode::DB_SAVE_ERROR,'操作失败');
    }

    /**
     * 获取当前用户组下的成员
     * @return false|string
     */
    public function getAdmin(){
        $size = $this->request->post('size', 10);
        $page = $this->request->post('page', 1);
        $gid = $this->request->post('gid', 0);
        if (!$gid) {
            return $this->buildFailed(ReturnCode::PARAM_INVALID, '非法操作');
        }
        $data = $this->admin->getAdmin($page,$size,$gid);
        return $this->buildSuccess($data);
    }

    /**
     * 更改管理员状态
     * @return false|string
     */
    public function adminStatus() {
        $id = $this->request->post('id');
        if(!$id)
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS, '参数错误');

        $status = $this->request->post('status');

        if ($this->admin->save(['status'=>$status],['id'=>$id]))
            return $this->buildSuccess([]);
        else
            return $this->buildFailed(ReturnCode::DB_SAVE_ERROR, '操作失败');
    }
}

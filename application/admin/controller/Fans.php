<?php
/**
 * Author: yann
 * QQ: 9197313
 * Date: 2018/11/28 0028
 * Time: 下午 2:25
 */

namespace app\admin\controller;


use app\model\MpFans;
use app\util\ReturnCode;

class Fans extends Base
{

    protected $mpFans;

    public function __construct(MpFans $mpFans)
    {
        parent::__construct();
        $this->mpFans = &$mpFans;
    }

    /**
     * 粉丝列表
     * @return string
     */
    public function getList(){
        $page = $this->request->post('page',1);
        $size = $this->request->post('size',20);
        $list = $this->mpFans->getList(1,$page,$size);
        return $this->buildSuccess($list);
    }

    /**
     * 删除粉丝
     * @return string
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function fansDel(){
        $id = $this->request->post('id');
        if(!$id)
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS,'参数错误');
        $this->mpFans->where(['id'=>$id])->delete();
        return $this->buildSuccess([]);
    }
}

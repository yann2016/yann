<?php
/**
 * Author: yann
 * QQ: 9197313
 * Date: 2018/12/26 0026
 * Time: 上午 9:30
 */

namespace app\api\controller;


use EasyWeChat\Factory;
use EasyWeChat\Kernel\Messages\Text;
use think\Controller;

class Mpserver extends Controller
{

    public function mpResponse()
    {
        try {
            $app = Factory::officialAccount(config('wechat.official_account.default'));
            $message = $app->server->getMessage();
            if(isset($message['FromUserName'])){
                $app->server->push(function ($message) {
                    switch ($message['MsgType']) {
                        case 'event':
                            return '收到事件消息';
                            break;
                        case 'text':
                            $re = $message['Content'];
                            $text = new Text($re.' yann');
                            return $text;
                            break;
                        case 'image':
                            return '收到图片消息';
                            break;
                        case 'voice':
                            return '收到语音消息';
                            break;
                        case 'video':
                            return '收到视频消息';
                            break;
                        case 'location':
                            return '收到坐标消息';
                            break;
                        case 'link':
                            return '收到链接消息';
                            break;
                        case 'file':
                            return '收到文件消息';
                            break;
                        // ... 其它消息
                        default:
                            return '收到其它消息';
                            break;
                    }

                });
            }
            $app->server->serve()->send();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function miniResponse()
    {
        try {
            $app = Factory::miniProgram(config('wechat.mini_program.default'));
            $message = $app->server->getMessage();
            if(isset($message['FromUserName'])){
                $re = $message['Content'];
                $openid = $message['FromUserName'];
                $text = new Text($re.' yann');
                $app->customer_service->message($text)->to($openid)->send();
            }else{
                $app->server->serve()->send();
            }

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //群发消息，一天一次
    public function sendMsg(){
        try{
            $app = Factory::officialAccount(config('wechat.official_account.default'));

            $res = $app->broadcasting->sendText("大家好！欢迎使用 EasyWeChat。");
            dump($res);
        }catch (\Exception $e){
            dump($e->getMessage());
        }
    }

    public function getIndustry(){
        $app = Factory::officialAccount(config('wechat.mini_program.default'));
        $res = $app->template_message->getIndustry();
        dump($res);
    }
}
